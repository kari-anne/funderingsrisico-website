CREATE TABLE public.paal AS (
  pandidentificatie varchar,
  bouwjaar integer,
  hoogste_grondwaterstand float,
  laagste_grondwaterstand float,
  risicolabel varchar(1),
  overbodig varchar(255),
  diepte_veenofkleilaag float,
  diepte_zandlaag float,
  grondwaterfluctuatie float
)

COPY public.paal FROM '/data/paal_data.csv' DELIMITERS ',' CSV;

CREATE TABLE public.staal (
  pandidentificatie varchar,
  bouwjaar integer,
  hoogste_grondwaterstand float,
  laagste_grondwaterstand float,
  overbodig varchar(255),
  diepte_veenofkleilaag float,
  risicolabel varchar(1),
  diepte_zandlaag float,
  grondwaterfluctuatie float
)

COPY public.staal FROM '/data/staal_data.csv' DELIMITERS ',' CSV;


ogr2ogr -f "GeoJSON" ./paal.json -sql "select * from paal_vt" PG:"host=127.0.0.1 user=karidepari dbname=ondergrondviewer_production password=karideparipassword port=5432" -progress
ogr2ogr -f "GeoJSON" ./staal.json -sql "select * from staal_vt" PG:"host=127.0.0.1 user=karidepari dbname=ondergrondviewer_production password=karideparipassword port=5432" -progress

