import { TestBed } from '@angular/core/testing';

import { BAGService } from './bag.service';

describe('BAGService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BAGService = TestBed.get(BAGService);
    expect(service).toBeTruthy();
  });
});
