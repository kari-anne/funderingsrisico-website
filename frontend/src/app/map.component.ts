import { Component, OnInit, AfterViewInit, ViewChild, Input, OnChanges, SimpleChanges } from '@angular/core'
import * as mapboxgl from 'mapbox-gl';
import { Map } from 'mapbox-gl';
import { MapInteractionService } from './map-interaction.service';

@Component({
    selector: 'app-map',
    template: `<div id="map" style="flex: 1;"></div> `,
})


export class MapComponent implements OnInit, OnChanges, AfterViewInit {
    private map: Map;
    @Input() public activeLayer: string;

    private paalLayer = {
        "id": "paal-layer",
        "type": "fill",
        "source": {
            "type": "vector",
            "tiles": ["http://116.203.153.96/data/paal/{z}/{x}/{y}.pbf"],
            "minzoom": 12,
            "maxzoom": 14
        },
        "source-layer": "paal",
        "paint": {
            "fill-color": [
                'match',
                ['get', 'risicolabel'],
                'A', '#006400',
                'B', '#32CD32',
                'C', '#FFD700',
                'D', '#FE5E01',
                'E', '#FF0000',
                /* other */ '#ccc'
            ],
            "fill-outline-color": '#000000',
            "fill-opacity": ['case',
                ['boolean', ['feature-state', 'hover'], false],
                ['case',
                    ['boolean', ['feature-state', 'selected'], false],
                    1.0,
                    0.85
                ],
                0.7
            ],
        }
    }

    private staalLayer = {
        "id": "staal-layer",
        "type": "fill",
        "source": {
            "type": "vector",
            "tiles": ["http://116.203.153.96/data/staal/{z}/{x}/{y}.pbf"],
            "minzoom": 12,
            "maxzoom": 14
        },
        "source-layer": "staal",
        "paint": {
            "fill-color": [
                'match',
                ['get', 'risicolabel'],
                'A', '#006400',
                'B', '#32CD32',
                'D', '#FE5E01',
                /* other */ '#ccc',
            ],
            "fill-outline-color": '#000000',
            "fill-opacity": ['case',
                ['boolean', ['feature-state', 'hover'], false],
                ['case',
                    ['boolean', ['feature-state', 'selected'], true],
                    1.0,
                    0.85
                ],
                0.7
            ],
        }
    }

    constructor(private mapInteractionService: MapInteractionService) {
    }

    ngOnInit() {
        console.log(this.activeLayer);
    }

    ngOnChanges(changes: SimpleChanges) {
        this.setActiveLayer(changes.activeLayer.currentValue);
    }

    ngAfterViewInit() {
        // console.log('hi from angular after view init')
        mapboxgl.accessToken = 'pk.eyJ1Ijoia2FyaS1hbm5lIiwiYSI6ImNqeXIxMHBneDA1d2IzYnFkeG00cnJpamUifQ.6YcYvVMgJsta9stP_tP1UQ';
        this.map = new mapboxgl.Map({
            container: 'map',
            zoom: 9,
            center: [5.099831, 52.083129],
            style: 'mapbox://styles/kari-anne/cjyr1ce5d337e1crsbbbgmw5j',
            attributionControl: false
        });

        this.map.on('load', () => {
            this.mapInteractionService.bounds$.subscribe((bounds) => {
                this.map.fitBounds(bounds, { maxZoom: 19 });
            })

            this.map.addLayer(this.paalLayer);
            this.map.addControl(new mapboxgl.NavigationControl(), 'bottom-right');
            this.mapInteractionService.mapLoaded.next(true);
        });

        this.map.on('click', 'paal-layer', (e) => {
            this.handleClick(e.features[0]);
        });

        this.map.on('click', 'staal-layer', (e) => {
            this.handleClick(e.features[0]);
        });

        this.map.on('mouseleave', 'paal-layer', (e) => {
            this.resetHoverState(this.paalLayer.source);
        });

        this.map.on('mousemove', 'paal-layer', (e) => {
            this.resetHoverState(this.paalLayer.source);
            if (e.features.length > 0) {
                this.setHoverState(e.features[0]);
            }
        });

        this.map.on('mouseleave', 'staal-layer', (e) => {
            this.resetHoverState(this.staalLayer.source);
        });

        this.map.on('mousemove', 'staal-layer', (e) => {
            this.resetHoverState(this.staalLayer.source);
            if (e.features.length > 0) {
                this.setHoverState(e.features[0]);
            }
        });
    }

    private handleClick(feature) {
        this.resetSelectedState(feature);
        this.setSelectedState(feature);
        this.mapInteractionService.setClick(feature);
    }

    private resetHoverState(source) {
        this.map.getCanvas().style.cursor = '';
        const features = this.map.queryRenderedFeatures(source);
        features.forEach(feature => {
            this.map.setFeatureState({
                source: feature.source,
                sourceLayer: feature.sourceLayer,
                id: feature.id
            }, {
                    hover: false,
                    selected: feature.state.selected
                }
            );
        })
    }

    private resetSelectedState(feature) {
        const features = this.map.queryRenderedFeatures(feature.source);
        features.forEach(feature => {
            this.map.setFeatureState({
                source: feature.source,
                sourceLayer: feature.sourceLayer,
                id: feature.id
            }, {
                    selected: false
                }
            );
        })
    }

    private setHoverState(feature) {
        this.map.getCanvas().style.cursor = 'pointer';
        this.map.setFeatureState({
            source: feature.source,
            sourceLayer: feature.sourceLayer,
            id: feature.id
        }, {
                hover: true,
                selected: feature.state.selected

            }
        );
    }

    private setSelectedState(feature) {
        this.map.setFeatureState({
            source: feature.source,
            sourceLayer: feature.sourceLayer,
            id: feature.id
        }, {
                selected: true
            }
        );
    }

    private setActiveLayer(value) {
        if (this.map) {
            if (value === 'paal') {
                this.map.addLayer(this.paalLayer);
                this.map.removeLayer(this.staalLayer.id);
                this.map.removeSource(this.staalLayer.id);
            } else if (value === 'staal') {
                this.map.addLayer(this.staalLayer);
                this.map.removeLayer(this.paalLayer.id);
                this.map.removeSource(this.paalLayer.id);
            }
        }
    }
}
