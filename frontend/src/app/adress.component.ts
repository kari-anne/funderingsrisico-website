import { Component, OnInit } from '@angular/core'
import { MapInteractionService } from './map-interaction.service';
import * as turf from '@turf/turf';

@Component({
  selector: 'app-adress',
  template: `
      <geocoder (placeFound)="onPlaceFound($event)" placeholder="Zoek op adres of postcode...">
    `

})
export class AdressComponent {
  constructor(private mapInteractionService: MapInteractionService) {

  }

  public onPlaceFound(place) {
    place.geometrie_ll = turf.bbox(place.geometrie_ll)
    this.mapInteractionService.setBounds(place.geometrie_ll);
  }

}