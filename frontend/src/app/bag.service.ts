import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as querystring from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class BAGService {

  private bagWfsUrl = 'https://geodata.nationaalgeoregister.nl/bag/wfs?';

  constructor(
    private http: HttpClient
  ) {

  }

  public getAdressOfBuilding(building: any) {
    const pandId = building.properties.identificatie;
    const params = {
      service: 'WFS',
      version: '2.0.0',
      typename: 'verblijfsobject',
      request: 'GetFeature',
      outputFormat: 'application/json',
      srsName: 'EPSG:4326',
      cql_filter: `pandidentificatie= ${pandId}`
    };
    return this.http.get(this.bagWfsUrl + querystring.stringify(params)).toPromise().then((verblijfsobjecten: any) => {
      const formattedAddresses = this.formatAdresses(verblijfsobjecten.features);
      return formattedAddresses;
    });
  }

  private formatAdresses(adresses) {
    const adressProperties = adresses.map(adress => adress.properties);

    const groupedAdresses = this.groupBy(adressProperties, 'openbare_ruimte' as string);
    const groupedAdressStats = [];
    for (const [openbare_ruimte, adresses] of Object.entries(groupedAdresses) as [string, any]) {
      const groupObj = {
        straatnaam: openbare_ruimte,
        huisnummers: adresses.map(adress => adress.huisnummer),
        min: null,
        max: null
      }
      groupObj.min = Math.min.apply(Math, groupObj.huisnummers);
      groupObj.max = Math.max.apply(Math, groupObj.huisnummers);
      groupedAdressStats.push(groupObj);

    }
    return groupedAdressStats;
  }

  private groupBy(arr, property) {
    return arr.reduce((memo, x) => {
      if (!memo[x[property]]) {
        memo[x[property]] = [];
      }
      memo[x[property]].push(x);
      return memo;
    }, {});
  }


}
