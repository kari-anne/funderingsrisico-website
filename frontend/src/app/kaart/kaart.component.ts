import { Component, OnInit, Input } from '@angular/core';
import { mapToMapExpression } from '@angular/compiler/src/render3/util';
import { group } from '@angular/animations';
import { Button } from 'protractor';
import { MapInteractionService } from '../map-interaction.service';

@Component({
  selector: 'app-kaart',
  templateUrl: './kaart.component.html',
  styleUrls: ['./kaart.component.scss']
})
export class KaartComponent implements OnInit {
  public activeLayer = 'paal';
  public legend = false;
  public mapLoaded: any;
  constructor(
    private mapInteractionService: MapInteractionService
  ) { }

  ngOnInit() {
    this.mapInteractionService.mapLoaded.subscribe(value => {
      this.mapLoaded = value;
    });
  }

  public onFoundationChange(event) {
    this.activeLayer = event.value;
  }

  toggleLegend() {
    this.legend = !this.legend;
  }

}
