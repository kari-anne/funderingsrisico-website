import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MapComponent } from './map.component';
import { AdressComponent } from './adress.component';
import { GeocoderModule } from 'angular-geocoder';
import { HeaderComponent } from './header/header.component';
import { KaartComponent } from './kaart/kaart.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import { MatDialogModule } from '@angular/material/dialog';
import { InfoDialogComponent } from './header/info-dialog/info-dialog.component';
import { RiskInfoComponent } from './risk-info/risk-info.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    AdressComponent,
    HeaderComponent,
    KaartComponent,
    InfoDialogComponent,
    RiskInfoComponent,
  ],

  imports: [
    BrowserModule,
    FormsModule,
    GeocoderModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatRadioModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    InfoDialogComponent
  ]
})
export class AppModule { }
