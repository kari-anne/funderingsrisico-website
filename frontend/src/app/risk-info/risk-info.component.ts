import { Component, OnInit } from '@angular/core';
import { MapInteractionService } from '../map-interaction.service';
import { InfoDialogComponent } from '../header/info-dialog/info-dialog.component';
import { runInThisContext } from 'vm';
import { BAGService } from '../bag.service';

@Component({
  selector: 'app-risk-info',
  templateUrl: './risk-info.component.html',
  styleUrls: ['./risk-info.component.scss'],
})

export class RiskInfoComponent {

  public building;
  public addresses;
  public construction_year;

  risk = 'Selecteer een pand';
  layer = 'paal';

  constructor(
    private mapInteractionService: MapInteractionService,
    private bagService: BAGService
  ) {
    this.mapInteractionService.click.subscribe(building => {
      this.onSelectedBuilding(building)
    });
  }

  public onSelectedBuilding(building) {
    console.log(building);
    this.building = building;
    this.bagService.getAdressOfBuilding(building).then((addresses) => {
      console.log(addresses);
      this.addresses = addresses
    });
    this.risk = building.properties.risicolabel
    this.layer = building.layer.id
    this.construction_year = building.properties.bouwjaar
  }
}



//     if (building.layer.id == 'paal-layer') {
//       console.log('paal')
//       if (building.properties.risicolabel == 'A') {
//         console.log('A')
//         this.risk = 'A'
//         this.layer = 'paal'
//       } else if (building.properties.risicolabel == 'B') {
//         console.log('B')
//         this.risk = 'B'
//         this.layer = 'paal'
//       } else if (building.properties.risicolabel == 'C') {
//         console.log('C')
//         this.risk = 'C'
//         this.layer = 'paal'
//       } else if (building.properties.risicolabel == 'D') {
//         console.log('D')
//         this.risk = 'D'
//         this.layer = 'paal'
//       } else if (building.properties.risicolabel == 'E') {
//         console.log('E')
//         this.risk = 'E'
//         this.layer = 'paal'
//       }
//     } else if (building.layer.id == 'staal-layer') {
//       console.log('staal')
//       if (building.properties.risicolabel == 'A') {
//         console.log('A')
//         this.risk = 'A'
//         this.layer = 'staal'
//       } else if (building.properties.risicolabel == 'B') {
//         console.log('B')
//         this.risk = 'B'
//         this.layer = 'staal'
//       } else if (building.properties.risicolabel == 'D') {
//         console.log('D')
//         this.risk = 'D'
//         this.layer = 'staal'
//       }
//     }
//   }
// }




