import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapInteractionService {
  public mapLoaded = new Subject();
  public bounds$: Subject<any> = new Subject();
  public buildingData: Subject<any> = new Subject();
  public paalOrStaal: Subject<any> = new Subject();
  public click: Subject<any> = new Subject();

  constructor() {
    this.mapLoaded.next(false);
  }

  public setBounds(bounds: any) {
    this.bounds$.next(bounds);
  }

  public handleClick(feature: any) {
    this.buildingData.next(feature);
  }

  public activeLayer(currentValue: any) {
    this.paalOrStaal.next(currentValue);
  }

  public setClick(click: any) {
    this.click.next(click);
  }

}
